# SCB using a CHERI ARM



## Objective

To build new ARM CHERI SBC's.  After multiple requests for Morello boards, we are unable to acquire one for research purposes.  Since we are a HW manufacturer, we will build and test and then manufacture to demand so that others can get these boards for R&D purposes. We also have a specific need.



## Status

Exploratory Phase:  We are still trying to determine the build and install base of the Capabilities Enhanced Morello boards.


## Authors and acknowledgment
Christopher Smolen is a lifelong Computer Engineer with a keen interest in Capabilites Enhanced Secutiry and typically works in C or Assembly at the bare metal level.

## License
https://creativecommons.org/licenses/by-sa/4.0/
